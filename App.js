import React, { useState, useEffect } from "react";
import { Text, View } from "react-native";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import Home from "./src/page/home";

import { Provider, connect } from "react-redux";
import { createStore, combineReducers } from "redux";
import reducer from "./src/reducers/quoteReducer";
const AppNavigator = createStackNavigator(
  {
    home: {
      screen: Home
    }
  },
  {
    initialRouteName: "home",
    headerMode: "none"
  }
);
const AppContainer = createAppContainer(AppNavigator);
let store = createStore(combineReducers({ quote: reducer }));
export default function App() {
  const [isReady, setIsReadyValue] = useState(false);

  useEffect(() => {
    setIsReadyValue(true);
  }, [isReady]);

  return (
    <View style={{ display: "flex", flex: 1 }}>
      {isReady ? (
        <Provider store={store}>
          <AppContainer />
        </Provider>
      ) : (
        <View>
          <Text>Loading...</Text>
        </View>
      )}
    </View>
  );
}
