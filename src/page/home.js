import React from "react"
import { View } from "react-native"
import HeaderComponent from "../components/header"
import Footer from "../components/footer"
import Scroll from '../components/scroll'
export default function Home() {
  return (
    <View style={{ display: "flex", flex: 1 }}>
      <View style={{ display: "flex" }}>
        <HeaderComponent />
      </View>
      <View style={{ display: "flex", flex: 1 }}>
        <Scroll/>
      </View>
      <View style={{ display: "flex" }}>
        <Footer />
      </View>
    </View>
  )
}
