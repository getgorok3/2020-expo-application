import React from "react";
import { View, Text } from "react-native";
import { Appbar, Button, IconButton } from "react-native-paper";
import { styles,colors } from "./styles";

export default function Footer() {
  return (
    <Appbar.Header style={{ ...styles.footer }}>
      <View style={{ ...styles.flex }}>
        <IconButton
          icon="magnify"
          color={colors.pink}
          size={32}
          onPress={() => console.log("Pressed")}
        />
      </View>
      <View style={{ ...styles.flex }}>
        <IconButton
          icon="home"
          color={colors.pink}
          size={32}
          onPress={() => console.log("Pressed")}
        />
      </View>
      <View style={{ ...styles.flex }}>
        <IconButton
          icon="plus-circle"
          color={colors.pink}
          size={32}
          onPress={() => console.log("Pressed")}
        />
      </View>
    </Appbar.Header>
  );
}
