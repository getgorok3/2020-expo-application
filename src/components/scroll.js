import React,{useContext, useReducer } from "react";
import { View, Text, SafeAreaView, ScrollView } from "react-native";
import { styles } from "../components/styles";
import {useSelector, useDispatch} from 'react-redux'

export default function Scroll(props) {
  const quote = useSelector(state => state.quote)
  
  return (
    <View style={{ ...styles.flexContainer }}>
      <ScrollView horizontal={true}>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>AAA</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>BBB</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>CCC</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>DDD</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>EEE</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>FFF</Text>
        </View>
        <View  style={{ width: 250, height: 250, ...styles.header }}>
        <Text>GGG</Text>
        </View>
      </ScrollView>
    </View>
  );
}
