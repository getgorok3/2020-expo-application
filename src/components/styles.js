import React from "react";

export const colors={
    pink: "#DE4A9B"
}
export const styles ={
    header:{
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer:{
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    headerTitle: {
        textAlign: 'center',
        color: colors.pink,
        fontSize: 24,
        fontWeight: "bold",
        width: '100%'
    },
    flex: {
        display: 'flex'
    },
    flexContainer: {
        display: "flex",
         flex: 1 
    }
}