import React from "react";
import { View, Text } from "react-native";
import { Appbar } from "react-native-paper";
import { styles } from "./styles";

export default function HeaderComponent() {
  return (
    <Appbar.Header style={{ ...styles.header }}>
      <Text style={{ ...styles.headerTitle }}>Cheer Up!</Text>
    </Appbar.Header>
  );
}
