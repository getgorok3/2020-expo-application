
import {
  ADD_QUOTE,
  EDIT_QUOTE,
  GET_QUOTE,
  GET_QUOTES_LIST,
  DELETE_QUOTE
} from "../constants/appConstants"

const DEFAULT_STATE = []

//addQuote add new quote to list
const addQuote = (state, payload) => {
  const arr = state.map(item => item)
  arr.push(payload)
  return arr
}

//getQuote get specific quote detail
const getQuote = (state, { index }) => {
  return state.map((item, targetIndex) => (targetIndex === index ? item : {}))
}

//getQuotessList
const getQuotessList = state => {
  if (!!state.length > 0) {
    return state
  } else {
    return []
  }
}

//editQuote edit specific quote
const editQuote = (state, { index, ...data }) => {
  return state.map((item, targetIndex) =>
    targetIndex === index ? data : item
  )
}

//deleteQuote delete specific quote
const deleteQuote = (state, { index }) => {
  return state.filter((item, targetIndex) => targetIndex !== index)
}

export default reducer = (state = DEFAULT_STATE, { type, payload }) => {
  switch (type) {
    case ADD_QUOTE:
      return addQuote(state, payload)
    case GET_QUOTE:
      return getQuote(state, payload)
    case GET_QUOTES_LIST:
      return getQuotessList(state, payload)
    case EDIT_QUOTE:
      return editQuote(state, payload)
    case DELETE_QUOTE:
      return deleteQuote(state)
    default:
      return state
  }
}
