
export const ADD_QUOTE = "ADD_QUOTE"
export const EDIT_QUOTE = "EDIT_QUOTE"
export const GET_QUOTE = "GET_QUOTE"
export const GET_QUOTES_LIST = "GET_QUOTES_LIST"
export const DELETE_QUOTE = "DELETE_QUOTE"
